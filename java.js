function validateForm(){ 
	var name = document.forms["form"]["name"].value;
	var length = document.forms["form"]["length"].value;
	var width = document.forms["form"]["width"].value;
	var height = document.forms["form"]["height"].value;
	var mode = document.forms["form"]["mode"].value;
	var type = document.forms["form"]["type"].value;
	var weight = (length*width*height)/5000;
	
	alert("Parcel Volumetric and Cost Calculator"
		+"\nCustomer name: "+name
		+"\nLength: "+ length + " cm"
		+"\nWidth: "+ width + " cm"
		+"\nHeight: "+ height + " cm"
        +"\nWeight: "+ weight + " kg"
		+"\nMode: "+ mode
        +"\nType: "+ type
		+"\nDelivery cost: RM "+calculateCost(weight));
}
function resetAlert() {
	var reset= document.forms["form"]["reset"].value;
	alert("The input will be reset");
}

function upperCase() {
	var name= document.forms["form"]["name"];
	name.value = name.value.toUpperCase();
}

function calculateCost(weight) {
var mode = document.forms["form"]["mode"].value;
var type = document.forms["form"]["type"].value;
var cost=0;
	if(type=="domestic"){
		if(weight<2){
			if(mode=="surface"){
				cost=cost+7;
			}
			else if(mode=="air"){
				cost=cost+10;
			}
		}
		else if(weight>=2){
			if(mode=="surface"){
				cost=cost+7+((weight-2)*1.5);
			}
			else if(mode=="air"){
				cost=cost+10+(3*(weight-2)*3);
			}
		}
	}
	else if(type=="international"){
		if(weight<2){
			if(mode=="surface"){
				cost=cost+20;
			}
			else if(mode=="air"){
				cost=cost+50;
			}
		}
		else if(weight>=2){
			if(mode=="surface"){
				cost=cost+20+((weight-2)*3);
			}
			else if(mode=="air"){
				cost=cost+50+((weight-2)*5);
			}
		}
	}

	return cost;
}
